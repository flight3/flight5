# README

The API provided is [JSONAPI](http://jsonapi.org/format/)  compliant, please have a look at http://blog.arkency.com/2016/02/how-and-why-should-you-use-json-api-in-your-rails-api/


# Hacking

if you `rake db:seed` you will have two companies and a few airplanes.

To see whas happening, here is an example comapny and flight plan
`curl -v -H 'Authorization: Token token=b4air' localhost:3000/flightplans/1`.

Now, lets move this flightplan... from 'created' to 'subitted'
