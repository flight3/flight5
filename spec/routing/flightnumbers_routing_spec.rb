require "rails_helper"

RSpec.describe FlightnumbersController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/flightnumbers").to route_to("flightnumbers#index")
    end

    it "routes to #new" do
      expect(:get => "/flightnumbers/new").to route_to("flightnumbers#new")
    end

    it "routes to #show" do
      expect(:get => "/flightnumbers/1").to route_to("flightnumbers#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/flightnumbers/1/edit").to route_to("flightnumbers#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/flightnumbers").to route_to("flightnumbers#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/flightnumbers/1").to route_to("flightnumbers#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/flightnumbers/1").to route_to("flightnumbers#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/flightnumbers/1").to route_to("flightnumbers#destroy", :id => "1")
    end

  end
end
