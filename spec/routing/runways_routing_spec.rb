require "rails_helper"

RSpec.describe RunwaysController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/runways").to route_to("runways#index")
    end

    it "routes to #new" do
      expect(:get => "/runways/new").to route_to("runways#new")
    end

    it "routes to #show" do
      expect(:get => "/runways/1").to route_to("runways#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/runways/1/edit").to route_to("runways#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/runways").to route_to("runways#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/runways/1").to route_to("runways#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/runways/1").to route_to("runways#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/runways/1").to route_to("runways#destroy", :id => "1")
    end

  end
end
