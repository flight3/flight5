FactoryGirl.define do
  factory :user do
    name "airlineone"
    email "one@example.com"
    api_key "test"
  end
end
