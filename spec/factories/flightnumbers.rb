FactoryGirl.define do
  factory :flightnumber do
    number "MyString"
    departure_aerodrome "MyString"
    arrival_aerodrome "MyString"
  end
end
