FactoryGirl.define do
  factory :aircraft do
    identification "MyString"
    type_designator "MyString"
  end
end
