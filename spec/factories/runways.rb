FactoryGirl.define do
  factory :runway do
    number ""
    course ""
    lat ""
    lon ""
    elevation ""
    length ""
    ils false
  end
end
