FactoryGirl.define do
  factory :eddk do
    name "Koln Bonn"
    city "Cologne"
    country "Germany"
    iata "CGN"
    icao "EDDK"
    lat "50.865917"
    lon "7.142744"
    alt 302
    utc_offset 1
    dst "E"
    tz "Europe/Berlin"
  end
  factory :eddf do
    name "Frankfurt Main"
    city "Frankfurt"
    country "Germany"
    iata "FRA"
    icao "EDDF"
    lat "50.026421"
    lon "8.543125"
    alt 364
    utc_offset 1
    dst "E"
    tz "Europe/Berlin"
  end
end
