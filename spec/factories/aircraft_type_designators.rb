FactoryGirl.define do
  factory :aircraft_type_designator do
    icao "MyString"
    iata "MyString"
    description "MyString"
  end
end
