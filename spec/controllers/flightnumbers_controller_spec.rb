require 'rails_helper'

RSpec.describe FlightnumbersController, type: :controller do

  # This should return the minimal set of attributes required to create a valid
  # Flightnumber. As you add validations to Flightnumber, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    { number: "tst001", departure_aerodrome: "XAAA", arrival_aerodrome: "XZZZ", company_id: 1 }
  }

  let(:invalid_attributes) {
    { departure_aerodrome: "XAAA", arrival_aerodrome: "XZZZ" }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # FlightnumbersController. Be sure to keep this updated too.
  let(:valid_session) {
    {
      'Authorization': 'Token token=b4air'
    }
  }

  describe "GET #index" do
    it "assigns all flightnumbers as @flightnumbers" do
      flightnumber = Flightnumber.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:flightnumbers)).to eq([flightnumber])
    end
  end

  describe "GET #show" do
    it "assigns the requested flightnumber as @flightnumber" do
      flightnumber = Flightnumber.create! valid_attributes
      get :show, params: {id: flightnumber.to_param}, session: valid_session
      expect(assigns(:flightnumber)).to eq(flightnumber)
    end
  end

  describe "GET #new" do
    it "assigns a new flightnumber as @flightnumber" do
      get :new, params: {}, session: valid_session
      expect(assigns(:flightnumber)).to be_a_new(Flightnumber)
    end
  end

  describe "GET #edit" do
    it "assigns the requested flightnumber as @flightnumber" do
      flightnumber = Flightnumber.create! valid_attributes
      get :edit, params: {id: flightnumber.to_param}, session: valid_session
      expect(assigns(:flightnumber)).to eq(flightnumber)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Flightnumber" do
        expect {
          post :create, params: {flightnumber: valid_attributes}, session: valid_session
        }.to change(Flightnumber, :count).by(1)
      end

      it "assigns a newly created flightnumber as @flightnumber" do
        post :create, params: {flightnumber: valid_attributes}, session: valid_session
        expect(assigns(:flightnumber)).to be_a(Flightnumber)
        expect(assigns(:flightnumber)).to be_persisted
      end

      it "redirects to the created flightnumber" do
        post :create, params: {flightnumber: valid_attributes}, session: valid_session
        expect(response).to redirect_to(Flightnumber.last)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved flightnumber as @flightnumber" do
        post :create, params: {flightnumber: invalid_attributes}, session: valid_session
        expect(assigns(:flightnumber)).to be_a_new(Flightnumber)
      end

      it "re-renders the 'new' template" do
        post :create, params: {flightnumber: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested flightnumber" do
        flightnumber = Flightnumber.create! valid_attributes
        put :update, params: {id: flightnumber.to_param, flightnumber: new_attributes}, session: valid_session
        flightnumber.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested flightnumber as @flightnumber" do
        flightnumber = Flightnumber.create! valid_attributes
        put :update, params: {id: flightnumber.to_param, flightnumber: valid_attributes}, session: valid_session
        expect(assigns(:flightnumber)).to eq(flightnumber)
      end

      it "redirects to the flightnumber" do
        flightnumber = Flightnumber.create! valid_attributes
        put :update, params: {id: flightnumber.to_param, flightnumber: valid_attributes}, session: valid_session
        expect(response).to redirect_to(flightnumber)
      end
    end

    context "with invalid params" do
      it "assigns the flightnumber as @flightnumber" do
        flightnumber = Flightnumber.create! valid_attributes
        put :update, params: {id: flightnumber.to_param, flightnumber: invalid_attributes}, session: valid_session
        expect(assigns(:flightnumber)).to eq(flightnumber)
      end

      it "re-renders the 'edit' template" do
        flightnumber = Flightnumber.create! valid_attributes
        put :update, params: {id: flightnumber.to_param, flightnumber: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested flightnumber" do
      flightnumber = Flightnumber.create! valid_attributes
      expect {
        delete :destroy, params: {id: flightnumber.to_param}, session: valid_session
      }.to change(Flightnumber, :count).by(-1)
    end

    it "redirects to the flightnumbers list" do
      flightnumber = Flightnumber.create! valid_attributes
      delete :destroy, params: {id: flightnumber.to_param}, session: valid_session
      expect(response).to redirect_to(flightnumbers_url)
    end
  end

end
