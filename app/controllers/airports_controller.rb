class AirportsController < ApplicationController
  before_action :set_airport, only: [:show, :update, :destroy]

  # GET /airports
  def index
    @airports = Array.new

    if (params.has_key?(:icao))
      @airports = Airport.where("icao = ?", params[:icao].upcase!)
    elsif (params.has_key?(:iata))
      @airports = Airport.where("iata = ?", params[:iata].upcase!)
    elsif (params.has_key?(:q))
      logger.info "someone is looking for a full text index search"
    else
      @airports = Airport.all
    end

    render json: @airports
  end

  # GET /airports/1
  def show
    render json: @airport
  end

  # POST /airports
  def create
    @airport = Airport.new(airport_params)

    if @airport.save
      render json: @airport, status: :created, location: @airport
    else
      render json: @airport.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /airports/1
  def update
    if @airport.update(airport_params)
      render json: @airport
    else
      render json: @airport.errors, status: :unprocessable_entity
    end
  end

  # DELETE /airports/1
  def destroy
    @airport.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_airport
      if (params[:id].is_a? String)
        if (params[:id].length == 4)
          @airport = Airport.where("icao = ?", params[:id].upcase!)
        else
          @airport = Airport.where("iata = ?", params[:id].upcase!)
        end
      else
        @airport = Airport.find(params[:id])
      end
    end

    # Only allow a trusted parameter "white list" through.
    def airport_params
      params.require(:airport).permit(:name, :city, :country, :iata, :icao, :lat, :lon, :alt, :utc_offset, :dst, :tz)
    end
end
