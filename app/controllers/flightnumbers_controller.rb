class FlightnumbersController < ApplicationController
  before_action :set_flightnumber, only: [:show, :update, :destroy]

  # GET /flightnumbers
  def index
    @flightnumbers = Array.new

    if (params.has_key?(:departure_aerodrome))
      @flightnumbers = Flightnumber.where("departure_aerodrome = ?", params[:departure_aerodrome].upcase!)
    elsif (params.has_key?(:arrival_aerodrome))
      @flightnumbers = Flightnumber.where("arrival_aerodrome = ?", params[:arrival_aerodrome].upcase!)
    elsif (params.has_key?(:q))
      logger.info "someone is looking for a full text index search"
    else
      @flightnumbers = Flightnumber.all
    end

    render json: @flightnumbers
  end

  # GET /flightnumbers/1
  def show
    render json: @flightnumber
  end

  # POST /flightnumbers
  def create
    @flightnumber = Flightnumber.new(flightnumber_params)

    if @flightnumber.save
      render json: @flightnumber, status: :created, location: @flightnumber
    else
      render json: @flightnumber.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /flightnumbers/1
  def update
    if @flightnumber.update(flightnumber_params)
      render json: @flightnumber
    else
      render json: @flightnumber.errors, status: :unprocessable_entity
    end
  end

  # DELETE /flightnumbers/1
  def destroy
    @flightnumber.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_flightnumber
      @flightnumber = Flightnumber.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def flightnumber_params
      params.require(:flightnumber).permit(:number, :departure_aerodrome, :arrival_aerodrome)
    end
end
