class HealthzController < ApplicationController
  skip_before_action :authenticate

  def status
    render json: {data: [{type: "healthz", id: 1, attributes: {message: "All Systems GO!", version: "0.5.0", timeOfProcessing: Time.now.getutc.to_i }}]}
  end
end
