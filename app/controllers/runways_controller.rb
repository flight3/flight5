class RunwaysController < ApplicationController
  before_action :set_runway, only: [:show, :update, :destroy]

  # GET /runways
  def index
    @runways = Runway.all

    render json: @runways
  end

  # GET /runways/1
  def show
    render json: @runway
  end

  # POST /runways
  def create
    @runway = Runway.new(runway_params)

    if @runway.save
      render json: @runway, status: :created, location: @runway
    else
      render json: @runway.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /runways/1
  def update
    if @runway.update(runway_params)
      render json: @runway
    else
      render json: @runway.errors, status: :unprocessable_entity
    end
  end

  # DELETE /runways/1
  def destroy
    @runway.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_runway
      @runway = Runway.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def runway_params
      params.require(:runway).permit(:number, :course, :lat, :lon, :elevation, :length, :ils)
    end
end
