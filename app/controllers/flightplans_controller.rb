class FlightplansController < ApplicationController
  skip_before_action :authenticate
  before_action :authenticate_as_gfc, only: [:approve, :reject, :start, :finish]
  before_action :authenticate

  before_action :set_flightplan, only: [:show, :update, :destroy, :submit, :cancel, :approve, :reject, :start, :finish]

  # GET /flightplans
  def index
    @flightplans = Array.new

    if (params.has_key?(:state))
      @flightplans = Flightplan.where("UPPER(aasm_state) = ?", params[:state].upcase!)
    else
      @flightplans = Flightplan.all
    end

    render json: @flightplans
  end

  # GET /flightplans/1
  def show
    render json: @flightplan
  end

  # POST /flightplans
  def create
    @flightplan = Flightplan.new(flightplan_params)

    if @flightplan.save
      render json: @flightplan, status: :created, location: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /flightplans/1
  def update
    if @flightplan.update(flightplan_params)
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/submit
  def submit
    begin
      @flightplan.submit
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/cancel
  def cancel
    begin
      @flightplan.cancel
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/approve
  def approve
    begin
      @flightplan.approve
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/reject
  def reject
    begin
      @flightplan.reject
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/start
  def start
    begin
      @flightplan.start
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # PUT /flightplans/1/finish
  def finish
    begin
      @flightplan.finish
    rescue AASM::InvalidTransition => error
      render json: @flightplan.errors, status: :unprocessable_entity
      return
    end

    if @flightplan.save!
      render json: @flightplan
    else
      render json: @flightplan.errors, status: :unprocessable_entity
    end
  end

  # TODO check if we can refactor the last 6 methods into one, DRY!

  # DELETE /flightplans/1
  def destroy
    @flightplan.destroy
  end

  protected

  # Authenticate the user with token based authentication
  def authenticate_as_gfc
    authenticate_as_gfc_token || render_unauthorized
  end

  def authenticate_as_gfc_token
    authenticate_or_request_with_http_token('Global Flight Control') do |token, options|
      @current_user = User.find_by(name: "Global Flight Control")
      logger.debug "authenticate_as_gfc_token: #{@current_user.inspect}, token: #{token}, #{(@current_user.api_key.equal? token)}"
      return (@current_user.api_key == token.to_s)
    end
  end

  def render_unauthorized(realm = "Application")
    self.headers["WWW-Authenticate"] = %(Token realm="#{realm.gsub(/"/, "")}")
    render json: 'Bad credentials', status: :unauthorized
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_flightplan
    @flightplan = Flightplan.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def flightplan_params
    params.require(:flightplan).permit(:date_of_flight, :aircraft_identification, :departure_aerodrome, :departure_time, :cruising_speed, :cruising_speed_unit, :cruising_level, :cruising_level_unit, :arrival_aerodrome, :estimated_enroute_time, :persons_on_board)
  end
end
