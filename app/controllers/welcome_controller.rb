class WelcomeController < ApplicationController
  skip_before_action :authenticate
  
  def index
    render html: "This is just an API"
  end
end
