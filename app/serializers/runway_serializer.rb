class RunwaySerializer < ActiveModel::Serializer
  attributes :id, :number, :course, :lat, :lon, :elevation, :length, :ils
end
