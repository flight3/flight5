class FlightplanSerializer < ActiveModel::Serializer
  attributes :company, :id, :aasm_state, :date_of_flight, :aircraft_identification,
             :departure_aerodrome,
             :departure_time, :cruising_speed, :cruising_speed_unit, :cruising_level, :cruising_level_unit,
             :arrival_aerodrome,
             :estimate_enroute_time, :estimated_enroute_time, :distance
             :persons_on_board

  belongs_to :company
end
