class CompanySerializer < ActiveModel::Serializer
  belongs_to :user

  has_many :aircrafts
  has_many :flightnumbers

  attributes :id, :name, :aircrafts, :assets, :liabilities
end
