class AircraftSerializer < ActiveModel::Serializer
  belongs_to :company

  attributes :id, :identification, :type_designator

end
