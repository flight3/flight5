class FlightnumberSerializer < ActiveModel::Serializer
  attributes :id, :number, :departure_aerodrome, :arrival_aerodrome
end
