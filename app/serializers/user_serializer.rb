class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :password, :api_key

  has_one :company
end
