class AirportSerializer < ActiveModel::Serializer
#  has_many :runways

  attributes :id, :name, :city, :country, :iata, :icao, :lat, :lon, :alt, :utc_offset, :dst, :tz
end
