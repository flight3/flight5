class Company < ApplicationRecord
  belongs_to :user

  has_many :aircrafts
  has_many :flightnumbers
end
