class Flightnumber < ApplicationRecord
  validates :departure_aerodrome, :arrival_aerodrome, :presence => true

  validates_length_of :departure_aerodrome, :maximum => 4, :minimum => 4
  validates_length_of :arrival_aerodrome, :maximum => 4, :minimum => 4

  scope :owned_by, -> (company) { where(company_id: company.id) }

  private
  def uppercase_icao_and_iata
    self.departure_aerodrome.upcase!
    self.arrival_aerodrome.upcase!
  end
end
