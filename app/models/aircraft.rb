class Aircraft < ApplicationRecord
  belongs_to :company

  before_validation :uppercase_identification_type_designator

  validates :identification, uniqueness: true

  scope :owned_by, -> (company) { where(company_id: company.id) }

  private
  def uppercase_identification_type_designator
    identification.upcase!
    unless type_designator == nil
      type_designator.upcase!
    end
  end
end
