class Flightplan < ApplicationRecord
  include AASM

  aasm do
    after_all_transitions :log_status_change

    state :created, :initial => true
    state :submitted, :approved, :canceled, :rejected, :started, :finished

    event :submit do
      transitions :from => :created, :to => :submitted
    end

    event :approve do
      transitions :from => :submitted, :to => :approved
    end

    event :reject do
      transitions :from => :submitted, :to => :rejected
    end

    event :cancel do
      transitions :from => [ :submitted, :approved, :rejected ], :to => :canceled
    end

    event :start do
      transitions :from => [ :approved ], :to => :started
    end

    event :finish do
      transitions :from => [ :started ], :to => :finished
    end
  end

  belongs_to :company

  scope :created_by, -> (company) { where(company_id: company.id) }

  # calculate the distrance between departure_aerodrome and arrival_aerodrome
  # returned number is in meters
  def distance
    departure_aerodrome_loc = Airport.find_by(icao: departure_aerodrome)
    arrival_aerodrome_loc = Airport.find_by(icao: arrival_aerodrome)

    lat1 = departure_aerodrome_loc.lat.to_f * Math::PI / 180
    lat2 = arrival_aerodrome_loc.lat.to_f * Math::PI / 180
    deltalat = (arrival_aerodrome_loc.lat.to_f - departure_aerodrome_loc.lat.to_f) * Math::PI / 180
    deltalon = (arrival_aerodrome_loc.lon.to_f - departure_aerodrome_loc.lon.to_f) * Math::PI / 180

    a = Math.sin(deltalat/2) * Math.sin(deltalat/2) +
    Math.cos(lat1) * Math.cos(lat2) *
    Math.sin(deltalon/2) * Math.sin(deltalon/2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    d = 6371000 * c / 1000;

    return d
  end

  def estimate_enroute_time
    if not estimated_enroute_time.nil?
      return estimated_enroute_time
    end

    if estimated_enroute_time.nil? and not cruising_speed.nil?
      return distance / cruising_speed
    end
  end

  def log_status_change
    puts "changing from #{aasm.from_state} to #{aasm.to_state} (event: #{aasm.current_event})"
  end

  # TODO validate that aircraft_identification is valid eg an existing aircraft
  # TODO validate cruising_speed_unit is in ["kts", "kph"]
  # TODO cruising_level_unit is in ["ft", "m"]
end
