class Airport < ApplicationRecord
  validates :icao, :iata, :presence => true
  validates :icao, :uniqueness => true
  validates_length_of :icao, :maximum => 4, :minimum => 4
  validates_length_of :iata, :maximum => 3, :minimum => 3

  before_save :uppercase_icao_and_iata

  has_many :runways

  private
  def uppercase_icao_and_iata
    self.icao.upcase!
    self.iata.upcase!
  end
end
