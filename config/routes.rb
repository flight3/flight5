Rails.application.routes.draw do
  resources :flightnumbers
  get 'welcome/index'

  resources :users
  resources :companies
  resources :airports
  resources :runways
  resources :aircrafts
  resources :flightplans
  put '/flightplans/:id/submit', to: 'flightplans#submit'
  put '/flightplans/:id/cancel', to: 'flightplans#cancel'
  put '/flightplans/:id/reject', to: 'flightplans#reject'
  put '/flightplans/:id/approve', to: 'flightplans#approve'
  put '/flightplans/:id/start', to: 'flightplans#start'
  put '/flightplans/:id/finish', to: 'flightplans#finish'

  get '/healthz', to: 'healthz#status', as: 'healthz'

  root 'welcome#index'
end
