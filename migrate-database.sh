#!/bin/bash
set -e

echo "Executing bundle exec 'rake db:migrate' ..."
RAILS_ENV=production bundle exec rake db:migrate

if [ ! -z ${DO_SEED+x} ]
then
  echo "DO_SEED is set to '$DO_SEED', so I will rake db:seed now..."
  RAILS_ENV=production bundle exec rake db:seed
fi
