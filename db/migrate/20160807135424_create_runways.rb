class CreateRunways < ActiveRecord::Migration[5.0]
  def change
    create_table :runways do |t|
      t.string :number
      t.integer :course
      t.float :lat
      t.float :lon
      t.integer :elevation
      t.integer :length
      t.boolean :ils

      t.references :airport, foreign_key: true

      t.timestamps
    end
  end
end
