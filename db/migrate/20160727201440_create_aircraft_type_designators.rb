class CreateAircraftTypeDesignators < ActiveRecord::Migration[5.0]
  def change
    create_table :aircraft_type_designators do |t|
      t.string :icao
      t.string :iata
      t.string :description

      t.timestamps
    end
  end
end
