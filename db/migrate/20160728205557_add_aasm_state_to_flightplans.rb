class AddAasmStateToFlightplans < ActiveRecord::Migration
  def change
    add_column :flightplans, :aasm_state, :string
  end
end
