class AddAssetsLiabilitiesToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :assets, :integer
    add_column :companies, :liabilities, :integer
  end

  Company.find_each do |company|
    company.assets = 125000000
    company.liabilities = 0
    company.save!
  end
end
