class CreateAircrafts < ActiveRecord::Migration[5.0]
  def change
    create_table :aircrafts do |t|
      t.string :identification
      t.string :type_designator

      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
