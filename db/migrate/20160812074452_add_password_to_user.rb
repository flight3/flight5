class AddPasswordToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :password, :string

    User.find_each do |user|
      user.password = 'secret'
      user.save!
    end
  end
end
