class CreateAirports < ActiveRecord::Migration[5.0]
  def change
    create_table :airports do |t|
      t.string :name
      t.string :city
      t.string :country
      t.string :iata
      t.string :icao
      t.string :lat
      t.string :lon
      t.integer :alt
      t.integer :utc_offset
      t.string :dst
      t.string :tz

      t.timestamps
    end
  end
end
