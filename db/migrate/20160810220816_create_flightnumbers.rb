class CreateFlightnumbers < ActiveRecord::Migration[5.0]
  def change
    create_table :flightnumbers do |t|
      t.string :number
      t.string :departure_aerodrome, limit: 4
      t.string :arrival_aerodrome, limit: 4

      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
