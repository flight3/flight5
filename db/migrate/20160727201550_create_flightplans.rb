class CreateFlightplans < ActiveRecord::Migration[5.0]
  def change
    create_table :flightplans do |t|
      t.string :date_of_flight
      t.string :aircraft_identification
      t.string :departure_aerodrome
      t.datetime :departure_time
      t.integer :cruising_speed
      t.string :cruising_speed_unit
      t.integer :cruising_level
      t.string :cruising_level_unit
      t.string :arrival_aerodrome
      t.integer :estimated_enroute_time
      t.integer :persons_on_board

      t.references :company, foreign_key: true

      t.timestamps
    end
  end
end
