# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160812074452) do

  create_table "aircraft_type_designators", force: :cascade do |t|
    t.string   "icao"
    t.string   "iata"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "aircrafts", force: :cascade do |t|
    t.string   "identification"
    t.string   "type_designator"
    t.integer  "company_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["company_id"], name: "index_aircrafts_on_company_id"
  end

  create_table "airports", force: :cascade do |t|
    t.string   "name"
    t.string   "city"
    t.string   "country"
    t.string   "iata"
    t.string   "icao"
    t.string   "lat"
    t.string   "lon"
    t.integer  "alt"
    t.integer  "utc_offset"
    t.string   "dst"
    t.string   "tz"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.integer  "assets"
    t.integer  "liabilities"
  end

  create_table "flightnumbers", force: :cascade do |t|
    t.string   "number"
    t.string   "departure_aerodrome", limit: 4
    t.string   "arrival_aerodrome",   limit: 4
    t.integer  "company_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["company_id"], name: "index_flightnumbers_on_company_id"
  end

  create_table "flightplans", force: :cascade do |t|
    t.string   "date_of_flight"
    t.string   "aircraft_identification"
    t.string   "departure_aerodrome"
    t.datetime "departure_time"
    t.integer  "cruising_speed"
    t.string   "cruising_speed_unit"
    t.integer  "cruising_level"
    t.string   "cruising_level_unit"
    t.string   "arrival_aerodrome"
    t.integer  "estimated_enroute_time"
    t.integer  "persons_on_board"
    t.integer  "company_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "aasm_state"
    t.index ["company_id"], name: "index_flightplans_on_company_id"
  end

  create_table "runways", force: :cascade do |t|
    t.string   "number"
    t.integer  "course"
    t.float    "lat"
    t.float    "lon"
    t.integer  "elevation"
    t.integer  "length"
    t.boolean  "ils"
    t.integer  "airport_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["airport_id"], name: "index_runways_on_airport_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "api_key"
    t.string   "password"
  end

end
