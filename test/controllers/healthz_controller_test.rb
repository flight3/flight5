require 'test_helper'

class HealthzControllerTest < ActionDispatch::IntegrationTest
  test "should get status" do
    get healthz_url
    assert_response :success
  end

end
