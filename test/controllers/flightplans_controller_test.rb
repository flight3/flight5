require 'test_helper'

class FlightplansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @eddk = airports(:EDDK)
    @eddf = airports(:EDDF)
    @flightplan = flightplans(:one)
  end

  test "should get index" do
    get flightplans_url, headers: { "Authorization" => "Token token=test" }, as: :json
    assert_response :success
  end

end
